from layer import Layer
from math import log
from copy import deepcopy
class Network(object):
    def __init__(self, regularizationFactor=1, alpha=0.1, loopRate=0.1, logLimit=-1000):
        self.layers = []
        self.rFactor = regularizationFactor
        self.alpha = alpha ## gradiente descendente
        self.loopRate = loopRate ## laco do train
        self.logLimit = logLimit ## Valor caso log seja muito grande

    def addLayer(self, layer):
        self.layers.append(layer)

    def computeOutput(self, input):
        nextInput = input
        self.layers[0].inputs = input
        for layer in self.layers[1:]:
            layer.loadInputs(nextInput)
            nextInput = layer.getHiddenOutputs()
        return nextInput

    def computeError(self, correctOutput):
        nextCorrectOutput = [correctOutput]
        for layer in reversed(self.layers):
            layer.loadCorrectOutputs(nextCorrectOutput)
            nextCorrectOutput = layer.getCorrectInputs()
        return nextCorrectOutput[0]

    def train(self, inputArray, outputArray, printGradVerif=False, grafVerifEpsilon=0.000001): # not to be confused with a locomotive
        # inputArray: [[1,2,3], [10]],
        # outputArray: [[5,6], [7]]
        # means that input [1,2,3] has as output [5,6]
        nextJ = 1
        currentJ = 0

        while (abs(nextJ - currentJ) > self.loopRate) and nextJ < 10000: #and (nextJ - currentJ <= 1):
            currentJ = nextJ
            for i, inputt in enumerate(inputArray):
                correctOutput = outputArray[i]
                calculatedOutput = self.computeOutput(inputt)
                nextJ = self.J_final(inputArray, outputArray)
                
                self.computeDeltas(calculatedOutput, correctOutput)
                self.computeGradient()
                self.updateWeights()

        if (printGradVerif):
            self.J_PrintGradientVerification(grafVerifEpsilon, inputArray, outputArray)

    def computeDeltas(self, calculatedOutput, correctOutput):
        layersReversedList = self.layers[::-1]
        # layersReversedList[0].deltaOutputLayer(datasetOutput)# TODO define datasetOutput

        for i, layer in enumerate(layersReversedList):
            if i == len(layersReversedList)-1:
                continue
            layer.clearDelta()
            if(i == 0): #Step 2
                layer.deltaOutputLayer(calculatedOutput, correctOutput)
            else: #Step 3
                for k in range(1, len(layersReversedList[i-1].weights.matrix[0])): #0 is to get a col value, every col has the same size
                    multDeltaWeights = layersReversedList[i-1].getMultDeltaWeights(k)
                    activation = layer.getHiddenOutputs()
                    auxDelta = multDeltaWeights*activation[k-1]*(1-activation[k-1])
                    layer.loadDelta(auxDelta)

    def computeGradient(self):
        for k, layer in enumerate(self.layers[:-1]):
            if k == 0:
                activation = layer.inputs
            else: activation = layer.getHiddenOutputs()

            deltas = self.layers[k+1].getDelta()

            for i, delta in enumerate(deltas): #loads bias
                currentDelta = deltas[i]
                layer.loadGradient(0, i, currentDelta)

            for m, _ in enumerate(layer.weights.matrix):
                a = activation[m]

                for i, _ in enumerate(self.layers[k+1].weights.matrix):
                    currentDelta = deltas[i]
                    layer.loadGradient(m+1, i, a * currentDelta + self.rFactor * self.layers[k+1].weights.at(m, i))


    def updateWeights(self):
        for k, layer in enumerate(self.layers[1:]):
            layerWeights = layer.getWeights()
            layerGradients = self.layers[k].getGradients()

            for i, neuronWeights in enumerate(layerWeights.matrix):
                for j, _ in enumerate(neuronWeights):
                    layerWeights.setAt(j, i, layerWeights.at(j, i) - self.alpha * layerGradients.at(i,j))
    #
    # J FUNCTIONS
    #

    def log(self, num):
        try:
            result = log(num)
        except:
            result = self.logLimit
        return result

    def J_attrSummatory(self, calcOutputs, correctOutputs, n):
        summatory = 0
        # K is the amount of outputs
        K = len(calcOutputs)

        for k in range(K):
            y_k = correctOutputs[k]
            f_x_k = calcOutputs[k]
            term = - y_k * self.log(f_x_k) - (1 - y_k) * self.log(1 - f_x_k)
            summatory += term

        return summatory

    def J_weightSummatory(self):
        summatory = 0
        for layer in self.layers:
            for neuronWeights in layer.weights.matrix:
                for weight in neuronWeights[1:]:
                    summatory += weight * weight
        return summatory

    # Computes J for many input instances, used in testing
    def J_final(self, inputArray, outputArray):
        firstSummatory = 0
        for i, input in enumerate(inputArray):
            correctOutput = outputArray[i]
            calculatedOutput = self.computeOutput(input)
            firstSummatory += self.J_attrSummatory(calculatedOutput, correctOutput, len(inputArray))

        secondSummatory = self.J_weightSummatory()

        N = len(inputArray)
        finalJ = firstSummatory * 1/N + secondSummatory * self.rFactor/(2*N)
        return finalJ

    # Gradient verification
    def J_PrintGradientVerification(self, epsilon, inputArray, outputArray):
        networkCopyPlus = deepcopy(self)
        networkCopyMinus = deepcopy(self)
        for k, l in enumerate(networkCopyPlus.layers):
            for i, row in enumerate(l.weights.matrix):
                for j, _ in enumerate(row):
                    networkCopyPlus.layers[k].weights.matrix[i][j] += epsilon
                    networkCopyMinus.layers[k].weights.matrix[i][j] -= epsilon

                    Jplus = networkCopyPlus.J_final(inputArray, outputArray)
                    Jminus = networkCopyMinus.J_final(inputArray, outputArray)
                    J = Jplus - Jminus / (2 * epsilon)
                    if (i < len(l.gradients.matrix) and j < len(l.gradients.matrix[i])):
                        print ("Gradient calculation: ", l.gradients.matrix[i][j], "Gradient aproximation: ", J)

    #
    #
    #

    def predictOutcomeForNewEntry(self, entry):
        return self.computeOutput(entry)

    def printNetwork(self):
        print("NETWORK {")
        print("  rFACTOR " + str(self.rFactor))
        for i, layer in enumerate(self.layers):
            if i == 0:
                print("  INPUT LAYER {")
                for i in range(len(layer.weights.matrix)):
                    print("    NEURON " + str(i) + " {" + "}")
                print("  }")
            else:
                print("  LAYER " + str(i) + " {")
                layer.printLayer()
                print("  }")
        print("}")
