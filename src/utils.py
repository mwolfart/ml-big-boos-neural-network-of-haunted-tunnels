class Matrix(object):
    def __init__(self, nRows=1, nCols=1):
        #self.matrix = [[0] * nCols for i in range(nRows)]
        self.matrix = []
        for _ in range(nRows):
            row = []
            for _ in range(nCols):
                row.append(0)
            self.matrix.append(row)

    def at(self, col, row):
        return self.matrix[row][col]

    def setAt(self, col, row, value):
        self.matrix[row][col] = value

    def setFromArray(self, arr):
        self.matrix = arr

    def getColumns(self):
        return list(map(list, zip(*(self.matrix))))

    def getSize(self):
        return (len(self.matrix), len(self.matrix[0]))

    def multScalar(self, n):
        for row, _ in enumerate(self.matrix):
            for col, _ in enumerate(row):
                self.matrix[row, col] *= n

    ''' DEPRECATED
    def postMul(self, mat):
        if (len(self.matrix[0]) != len(mat)):
            raise "ERROR: Matrices have incompatible dimensions"
        else:
            product = []
            for row in self.matrix:
                newRow = []
                for col in mat.getColumns():
                    sum = 0
                    for i, val in enumerate(row):
                        sum += val * col[i]
                    newRow.append(sum)
                product.append(newRow)
            return product
    '''

    def postArrMul(self, arr):
        if (len(self.matrix[0]) != len(arr)):
            print(self.matrix, arr)
            raise "ERROR: Matrix and vector have incompatible dimensions"
        else:
            product = []
            for row in self.matrix:
                sum = 0
                for i, num in enumerate(arr):
                    sum += row[i] * num
                product.append(sum)
            return product

    def preArrMul(self, arr):
        if (len(self.matrix) != len(arr)):

            raise "ERROR: Matrix and vector have incompatible dimensions"
        else:
            product = []
            for row in self.getColumns():
                sum = 0
                for i, num in enumerate(arr):
                    sum += row[i] * num
                product.append(sum)
            return product

    #Given 2 arrays [x1,x2,x3...xn] and [y1,y2,y3...yn] returns a scalar of value x1 * y1 + x2 * y2 + x3 * y3... + xn*yn
    def weightedSum(self, arr1, arr2):
        if (len(arr1) != len(arr2)):
            print(arr2, arr1)
            raise "ERROR: Matrix and vector have incompatible dimensions"
        total = 0
        for i in range(len(arr1)):
            total += arr1[i] * arr2[i]
        return total