from utils import Matrix
import mlparser as parser

def expect(name, found, correct):
    if (found != correct):
        print("ERROR: Test {}: {} should be {}".format(name, found, correct))

def main():
    a = Matrix(3,3)
    b = Matrix(3,3)
    a.setFromArray([[1,2,3],[4,5,6],[7,8,9]])
    b.setFromArray([[1,2,2],[2,1,2],[2,2,1]])

    expect("Matrix access", a.matrix, [[1,2,3],[4,5,6],[7,8,9]])
    expect("Matrix transpose", a.getColumns(), [[1,4,7],[2,5,8],[3,6,9]])
    #expect("Matrix post multiplication", a.postMul(b), [[11,10,9],[26,25,24],[41,40,39]])

    ds = parser.readDataset("../data/dataset.txt")
    expect("Dataset parsing", ds, [([0.5, 0.7], [1.0, 0.0])])
    
    nw = parser.readNetwork("../data/network.txt")
    parser.readInitialWeights("../data/initial_weights.txt", nw)
    parser.saveNetworkWeights("../data/output_weights.txt", nw)
    # expect("Reading and writing weights", )

    nw.printNetwork()


if __name__ == "__main__":
    main()