def createBatches(dataset, nBatches):
    if nBatches == 1:
        return [dataset]
        
    batches = []
    for i in range(nBatches):
        batches.append([])
    i = 0

    for data in dataset:
        batches[i].append(data)
        i += 1

        if i == nBatches:
            i = 0

    return batches