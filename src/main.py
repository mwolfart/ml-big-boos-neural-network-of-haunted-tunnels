import network
import sys
import random
from mlparser import *
from cross import *
from batch import *
from error import *

# ./backpropagation network.txt initial_weights.txt dataset.txt

def normalizeDataset(dataset):
    nAttrs = len(dataset[0][0])
    nOutputs = len(dataset)
    minValuesIn = [0] * nAttrs
    maxValuesIn = [1] * nAttrs
    #minValuesOut = [0] * nOutputs
    #maxValuesOut = [1] * nOutputs

    for entry in dataset:
        for i, attr in enumerate(entry[0]):
            if (attr < minValuesIn[i]):
                minValuesIn[i] = attr
            if (attr > maxValuesIn[i]):
                maxValuesIn[i] = attr
        '''
        for i, out in enumerate(entry[1]):
            if (out < minValuesOut[i]):
                minValuesOut[i] = out
            if (out > maxValuesOut[i]):
                maxValuesOut[i] = out
        '''

    for entry in dataset:
        for i, attr in enumerate(entry[0]):
            entry[0][i] = (attr - minValuesIn[i])/(maxValuesIn[i] - minValuesIn[i])
        #for i, out in enumerate(entry[1]):
        #    entry[1][i] = (out - minValuesOut[i])/(maxValuesOut[i] - minValuesOut[i])


'''
DATASET FORMAT
[
    ([1,2,3], [5,4]),
    ([2,3,4], [5,7]),
    ([3,4,5], [3,4]),
    ...
]
'''

ALPHA = 0.0001
LOOPRATE = 0.05
LOGLIMIT = -10000

def main(argv):
    printGrad = False
    gradValue = 0.000001

    try:
        networkFile = argv[0]
        weightFile = argv[1]
        datasetFile = argv[2]
        if (len(argv) > 3):
            printGrad = argv[3]

        if (len(argv) > 4):
            gradValue = argv[4]
    except:
        print("Wrong syntax. Please use the following format:\n./backpropagation network.txt initial_weights.txt dataset.txt")

    network = readNetwork(networkFile, ALPHA, LOOPRATE, LOGLIMIT)
    readInitialWeights(weightFile, network)
    dataset = readDataset(datasetFile)
    normalizeDataset(dataset)
    random.shuffle(dataset)

    dataGroup = crossValidation(dataset, 3)
    #dataGroup = [[dataset, []]]        # for shorter datasets

    modelEfficiencies = []
    outcomeslist = []

    for pair in dataGroup:
        trainingSet = pair[0]
        testSet = pair[1]
        batches = createBatches(trainingSet, 3)

        for batch in batches:
            dAttr = list(map(lambda entry: entry[0], batch))
            dOutputs = list(map(lambda entry: entry[1], batch))
            network.train(dAttr, dOutputs, printGrad, gradValue)

        outcomes = calculateOutcomes(network, testSet, [1.0])
        outcomeslist.append(outcomes)
        efficiency = f1Measure(outcomes)
        modelEfficiencies.append(efficiency)

        network = readNetwork(networkFile, ALPHA, LOOPRATE, LOGLIMIT) ## RESET NETWORK
        readInitialWeights(weightFile, network)

    print("Outcomes: ", outcomeslist)
    print("Model efficiency: ", modelEfficiencies)

if __name__ == "__main__":
    main(sys.argv[1:])
