# (attributes, outcomes)
def calculateOutcomes(network, testSet, positiveValue):
    outcomes = {'VP': 0, 'FP': 0, 'VN': 0, 'FN': 0}

    for testEntry in testSet:
        predictedOutcome = network.predictOutcomeForNewEntry(testEntry[0])
        realOutcome = testEntry[1]

        #print(predictedOutcome, realOutcome)
        predictedOutcome[0] = round(predictedOutcome[0])
        #print(predictedOutcome, realOutcome)

        if (predictedOutcome == positiveValue and realOutcome == positiveValue):
            outcomes['VP'] += 1
        elif (predictedOutcome == positiveValue and realOutcome != positiveValue):
            outcomes['FP'] += 1
        elif (predictedOutcome != positiveValue and realOutcome == positiveValue):
            outcomes['FN'] += 1
        else:
            outcomes['VN'] += 1

    return outcomes

def f1Measure(outcomes):
    beta = 1
    prec = precision(outcomes)
    rev = recall(outcomes)
    if (prec == 0 and rev == 0):
        return 0
    else:
        return (1+beta**2) * (prec * rev / (beta**2 * prec + rev))

def precision(outcomes):
    if (outcomes['VP'] == 0 and outcomes['FP'] == 0):
        return 1
    else: return float(outcomes['VP'])/(outcomes['VP']+outcomes['FP'])

def recall(outcomes):
    if (outcomes['VP'] == 0 and outcomes['FN'] == 0):
        return 1
    else: return float(outcomes['VP'])/(outcomes['VP']+outcomes['FN'])
