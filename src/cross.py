from math import ceil
import copy

# #(data) = 10
# nFolds = 20
# each fold will have 1 entry
def crossValidation(data, nFolds):
    if len(data)/nFolds < 2:
        raise Exception("Data set too small for this amount of folds")
    elif nFolds < 2:
        raise Exception("ERROR: Amount of folds insufficient (must have at least 2 folds)")

    folds = getFolds(data, nFolds)

    dataGroup = []
    # [
    #  [training1, test1],
    #  [training2, test2],
    #  ...
    # ]
    for i in range(nFolds):
        dataGroup.append([[],[]])

    # dataGroup = [
    #   [trainingSet1, testSet1],
    #   [trainingSet2, testSet2],
    #   ...
    # ]

    # trainingSetX = [
    #   {'age': 5, 'income': 5},
    #   ...
    # ]

    for i, fold in enumerate(folds):
        testData = copy.deepcopy(fold)
        # nFolds is equivalent to the number of data groups we have
        # nFolds = len(dataGroup)
        for j in range(nFolds):
            # insert selected fold in all the other non-i dataGroups as training entry
            if j != i: dataGroup[j][0] += copy.deepcopy(fold)
        # for group i, insert as test (index 1)
        dataGroup[i][1] = testData

    return dataGroup

def getFolds(data, nFolds):
    # #(data) = 14
    # nFolds = 5
    # amountOfEntriesPerFold = ceil(14/5) = 3
    # {3} {3} {3} {3} {2}
    total = len(data)
    amountOfEntriesPerFold = ceil(total/nFolds)
    folds = []

    for i in range(nFolds):
        start = int(i*amountOfEntriesPerFold)
        end = int((i+1)*amountOfEntriesPerFold)

        if end > len(data):
            end = len(data)
        
        fold = data[start:end] # start inclusive, end exclusive
        folds.append(fold)

    return folds
