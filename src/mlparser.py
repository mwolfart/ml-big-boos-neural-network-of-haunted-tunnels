from layer import Layer
from network import Network
import re

def readNetwork(path, alpha, loopRate, logLimit):
    with open(path, 'r') as f:
        lines = f.readlines()
    
    rFactor = float(lines[0])
    network = Network(rFactor, alpha, loopRate, logLimit)

    prevLayerNo = 1
    for i, layerNo in enumerate(lines[1:]):
        if (i == len(lines)-2):
            nextLayerNo = 1
        else: nextLayerNo = lines[i+1+1]
        layer = Layer(int(layerNo), int(prevLayerNo), int(nextLayerNo))
        prevLayerNo = layerNo
        network.addLayer(layer)

    return network

def readInitialWeights(path, network):
    with open(path, 'r') as f:
        lines = f.readlines()

    for lId, layerInfo in enumerate(lines):
        neuronInfos = layerInfo.split('; ')
        for nId, neuronInfo in enumerate(neuronInfos):
            weightsForNeuron = neuronInfo.split(', ')
            for wId, weight in enumerate(weightsForNeuron):
                weight.strip()
                network.layers[lId+1].weights.setAt(wId, nId, float(weight))

def readDataset(path):
    with open(path, 'r') as f:
        lines = f.readlines()
    
    dataset = []
    for entry in lines:
        attributes, outputs = entry.split(';')
        attributes = list(map(lambda x:  float(x), attributes.split(', ')))
        outputs = list(map(lambda x:  float(x), outputs.split(', ')))
        dataset.append((attributes, outputs))

    return dataset

def saveNetworkWeights(path, network):
    with open(path, 'w') as f:
        for lId, layer in enumerate(network.layers):
            if lId == 0: continue
            nNeurons, nWeights = layer.weights.getSize()
            for neuron in range(nNeurons):
                for weight in range(nWeights):
                    f.write(str(layer.weights.at(weight, neuron)))
                    lastWeight = (weight < nWeights-1)
                    if lastWeight: f.write(', ')
                lastNeuron = (neuron < nNeurons-1)
                if lastNeuron: f.write('; ')
            f.write('\n')