from utils import Matrix
import math

class Layer(object):
    def __init__(self, nNeurons, nInputs, nOutputs):
        self.weights = Matrix(nNeurons, nInputs + 1)
        self.inputs = [1] + [0] * nInputs
        self.delta = []
        self.multDeltaWeights = []
        self.gradients = Matrix(nNeurons + 1, nOutputs) # +1 is from bias

    def getDelta(self):
        return self.delta
        
    def getOutputs(self):
        return self.weights.postArrMul(self.inputs)

    #DOES NOT GIVE BIAS
    def getHiddenOutputs(self): 
        outputs = self.getOutputs()
        hiddenOutputs = []
        for out in outputs:
            hiddenOutputs.append(self.activate(out))
        return hiddenOutputs        
        
    def loadInputs(self, inputs):
        self.inputs = [1] + inputs

    def getCorrectInputs(self):
        ## TODO error function
        return self.inputs

    def loadCorrectOutputs(self, correctOutpouts):
        self.correctOutpouts = correctOutpouts

    def getNumNeurons(self):
        return len(self.weights)

    def deltaOutputLayer(self, calculatedOutput, correctOutput):
        self.delta = [elemA - elemB for elemA, elemB in zip(calculatedOutput,correctOutput)]

    def getMultDeltaWeights(self, index):
        self.multDeltaWeights = self.weights.weightedSum(self.delta, [row[index] for row in self.weights.matrix])
        return self.multDeltaWeights

    def loadDelta(self, delta):
        self.delta.append(delta)

    def clearDelta(self):
        self.delta = []

    def loadGradient(self,i,j,gradient):
        self.gradients.setAt(j, i, gradient)

    def activate(self, input):
        if input < 0:
            return 1 - 1 / (1 + math.exp(input))
        return 1 / (1 + math.exp(-input))

    def loadWeight(self,i,j,weight):
        self.weights.setAt(j, i, weight)

    def getWeights(self):
        return self.weights

    def getGradients(self):
        return self.gradients        
        
    def printLayer(self):
        for i, neuronWeights in enumerate(self.weights.matrix):
            print("    NEURON " + str(i) + " {")
            for j, weight in enumerate(neuronWeights):
                print("      WEIGHT " + str(j) + ": " + str(weight) + (" (bias)" if j == 0 else ""))
            print("    }")